# Wirkk #
A WeeChat docker image to run on your server and connect to using SSH. Pre-configured with plugins and a theme.

## Installation ##
Install and run wirkk with `docker run -it -p 65005:65005 --rm -v /home/<YourUser>/.weechat:/home/wirkk/.weechat couchquid/wirkk`

This will launch wirkk with a volume for persistent data and a port for the webclient.

wirkk works great on a VPS([Vultr](https://www.vultr.com/?ref=7127950), [Digital Ocean](https://m.do.co/c/7a06d34d7dbc), [Linode](https://www.linode.com/?r=65f9a6e1ce5187febb45bd4537e22d55d21787d0))
